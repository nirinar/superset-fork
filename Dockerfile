FROM node:10-stretch AS src

ENV SUPERSET_HOME="/var/lib/superset/db"\
    SUPERSET_CONFIG_PATH="/var/lib/superset/local-config/superset_config.py"\
    SUPERSET_RELEASE=0.37

WORKDIR /var/lib
RUN git clone --single-branch -b main --depth 1 https://gitlab.com/nirinar/sie-mdg/superset.git superset


WORKDIR /var/lib/superset 
RUN touch invalidator.txt
RUN rm -rf .git
COPY scripts ./scripts/
COPY init ./init/
RUN chmod +x scripts/*.sh scripts/create-db.py
COPY config ./local-config/
RUN npm install -g po2json
#RUN po2json -d superset -f jed superset/translations/fr/LC_MESSAGES/messages.po superset/translations/fr/LC_MESSAGES/messages.json

WORKDIR /var/lib/superset/superset-frontend
RUN npm ci
COPY madagascar.geojson node_modules/@superset-ui/legacy-plugin-chart-country-map/esm/countries/madagascar.geojson
COPY madagascar_districts.geojson node_modules/@superset-ui/legacy-plugin-chart-country-map/esm/countries/madagascar_districts.geojson  
COPY modified/nv.d3.js  node_modules/nvd3/build/nv.d3.js 
COPY modified/controlPanel.js  node_modules/@superset-ui/legacy-plugin-chart-country-map/esm/controlPanel.js
COPY modified/countries.js  node_modules/@superset-ui/legacy-plugin-chart-country-map/esm/countries.js  
COPY modified/constants.js node_modules/@superset-ui/connection/esm/constants.js 
RUN npm run build
RUN rm -rf node_modules 

FROM python:3.6
ARG LDAP_HOST
ARG LDAP_URL
ARG LDAP_SEARCH
ARG LDAP_FILTER
ARG LDAP_BIND_USER
ARG LDAP_BIND_PASSWORD
ARG POSTGRES_PASSWORD
ARG SIEADM_PASSWORD
RUN echo $LDAP_HOST
RUN echo ${POSTGRES_PASSWORD:-}
ENV LDAP_HOST=${LDAP_HOST:-sie-madagascar.info}\
    LDAP_URL=${LDAP_URL:-ldap://ldap-server:389}\
    LDAP_SEARCH=${LDAP_SEARCH:-ou=People,{{LDAP_BASE_DN}}}\
    LDAP_FILTER=${LDAP_FILTER:-"(|(memberOf=cn=data_users,ou=Groups,{{LDAP_BASE_DN}})(memberOf=cn=data_admins,ou=Groups,{{LDAP_BASE_DN}}))"}\
    LDAP_BIND_USER=${LDAP_BIND_USER:-cn=admin,{{LDAP_BASE_DN}}}\
    LDAP_BIND_PASSWORD=${LDAP_BIND_PASSWORD:-t0pSecret!}\
    POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-}\
    SIEADM_PASSWORD=${SIEADM_PASSWORD:-t0pSecret!}\
    SUPERSET_HOME="/var/lib/superset/home"\
    SUPERSET_CONFIG_PATH="/var/lib/superset/local-config/superset_config.py"\
    SUPERSET_ENV=production\
    LANG=C.UTF-8\
    LC_ALL=C.UTF-8\
    USE_CACHE=0


COPY --from=src /var/lib/superset/ /var/lib/superset/

WORKDIR /var/lib/superset 
RUN sed  's/markupsafe==1.0/markupsafe==1.1/' requirements.txt > requirements.txt.changed && mv requirements.txt.changed  requirements.txt 
RUN sed  's/markupsafe==1.0/markupsafe==1.1/' requirements-dev.txt > requirements.txt.changed && mv requirements.txt.changed  requirements-dev.txt

RUN apt-get update\
    && apt-get install libsasl2-dev python-dev libldap2-dev libssl-dev -y\
    && rm -rf /var/lib/apt/lists/* \
    && pip install .\
    && pip install -r requirements.txt -r requirements-dev.txt gevent python-ldap python-redis\
    && flask fab babel-compile --target superset/translations\
    && sleep 10 

RUN touch invalidate19.txt
RUN chmod +x scripts/*.sh 


ENTRYPOINT ["/var/lib/superset/scripts/entrypoint.sh"]

HEALTHCHECK CMD ["curl", "-f", "http://localhost/health"]

EXPOSE 80

